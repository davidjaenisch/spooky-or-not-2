Template.postDisplay.helpers({
    imageFile: function(){
        return Images.findOne({_id: this.image});
    },
    votedClass: function(){
        var userId = Meteor.userId();
        if (userId && !_.include(this.upVoters, userId) && !_.include(this.downVoters, userId)){
            return 'spookyhover';
        } else {
            return 'disabled';
        }
    }
});

Template.postDisplay.events({
    'click .disabled': function(e) {
        swal("Please log in to vote!", "", "error")
    },
    'click .upvote': function(e) {
        e.preventDefault();
        Meteor.call('upvote', this._id);
    },
    'click .downvote': function(e) {
        e.preventDefault();
        Meteor.call('downvote', this._id);
    }
});
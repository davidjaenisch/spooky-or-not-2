Template.header.helpers({
    hasPosts: function(){
        try {
            check(Meteor.userId(), String);
        }
        catch(err) {
            return false;
        }
        var posts = Posts.find({
            createdBy: Meteor.userId()
        });
        return posts.count() > 0;
    }});
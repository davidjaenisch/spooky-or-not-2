Template.myPosts.helpers({
    imageFile: function(){
        return Images.findOne({_id: this.image});
    },
    ownPost: function() {
        return this.createdBy === Meteor.userId();
    }
})
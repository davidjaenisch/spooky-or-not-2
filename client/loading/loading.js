Template.loading.rendered = function () {
    if ( ! Session.get('loadingSplash') ) {
        this.loading = window.pleaseWait({
            logo: '/images/ghost_splash.png',
            backgroundColor: '#000000',
            loadingHtml: message + spinner
        });
        Session.set('loadingSplash', true); // just show loading splash once
    }
};

Template.loading.destroyed = function () {
    if ( this.loading ) {
        this.loading.finish();
    }
};

var message = '<p class="loading-message" >Welcome to Spooky Or Not</p>';
var spinner = '<div class="sk-spinner sk-spinner-rotating-plane"></div>';
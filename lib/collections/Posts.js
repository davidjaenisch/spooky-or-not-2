Posts = new Mongo.Collection("posts");
Posts.attachSchema(new SimpleSchema({
    //Post Info
    image: {
        type: String,
        autoform: {
            afFieldInput: {
                type: 'fileUpload',
                collection: 'Images'
            }
        },
        label: 'Choose file'
    },
    description: {
        type: String,
        label: "Description"
    },
    //User and Sorting Info
    createdBy: {
        type: String,
        autoform: {
            omit: true
        },
        autoValue: function () {
            if (this.isInsert && (!this.isSet || this.value.length === 0)) {
                return this.userId
            }
        }
    },
    submitted: {
        type: Date,
        autoform: {
            omit: true
        },
        autoValue: function () {
            if (this.isInsert && (!this.isSet || this.value.length === 0)) {
                return new Date()
            }
        }
    },
    upVoters: {
        type: [String],
        autoform: {
            omit: true
        },
        autoValue: function () {
            if (this.isInsert && (!this.isSet || this.value.length === 0)) {
                return [];
            }
        }
    },
    upVotes: {
        type: Number,
        autoform: {
            omit: true
        },
        autoValue: function () {
            if (this.isInsert && (!this.isSet || this.value.length === 0)) {
                return 0;
            }
        }
    },
    downVoters: {
        type: [String],
        autoform: {
            omit: true
        },
        autoValue: function () {
            if (this.isInsert && (!this.isSet || this.value.length === 0)) {
                return [];
            }
        }
    },
    downVotes: {
        type: Number,
        autoform: {
            omit: true
        },
        autoValue: function () {
            if (this.isInsert && (!this.isSet || this.value.length === 0)) {
                return 0;
            }
        }
    }
}));

Posts.allow({
    insert: function () {
        return true;
    },
    update: function (userId, post) {
        return ownsDocument(userId, post);
    },
    remove: function (userId, post) {
        return ownsDocument(userId, post);
    }
});

Meteor.methods({
    upvote: function (postId) {
        check(this.userId, String);
        check(postId, String);
        var affected = Posts.update({
            _id: postId,
            upVoters: {$ne: this.userId},
            downVoters: {$ne: this.userId}
        }, {
            $addToSet: {upVoters: this.userId},
            $inc: {upVotes: 1}
        });
        if (! affected){
            throw new Meteor.Error('invalid', "You weren't able to upvote that post");
        }
    },
    downvote: function (postId) {
        check(this.userId, String);
        check(postId, String);
        var affected = Posts.update({
            _id: postId,
            upVoters: {$ne: this.userId},
            downVoters: {$ne: this.userId}
        }, {
            $addToSet: {downVoters: this.userId},
            $inc: {downVotes: 1}
        });
        if (! affected){
            throw new Meteor.Error('invalid', "You weren't able to upvote that post");
        }
    }
})
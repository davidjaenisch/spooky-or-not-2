Images = new FS.Collection("images",{
        stores: [new FS.Store.GridFS("images", {})]
    }
);

Images.allow({
    download: function(userId, doc){
        return true;
    },
    insert: function(userId){
        return true;
    },
    update: function(userId, doc){
        return true;
    }
});
Router.configure({
    layoutTemplate: 'layout',
    loadingTemplate: 'loading',
    notFoundTemplate: 'notFound',
    waitOn: function () {
        Meteor.subscribe('myPosts', Meteor.userId());
    }
});
var requireLogin = function () {
    if (!Meteor.user()) {
        this.render('accessDenied');
    } else {
        this.next();
    }
};
Router.onBeforeAction(requireLogin, {only: 'createPost'});
Router.route('/submit',
    {
        name: 'createPost',
        waitOn: function () {
            return Meteor.subscribe('images');
        }
    });

Router.route('/posts/:_id/edit', {
    name: 'editPost',
    waitOn: function () {
        Meteor.subscribe('singlePost', this.params._id);
        Meteor.subscribe('images');
    },
    data: function () {
        return Posts.findOne(this.params._id);
    }
});

PostsListController = RouteController.extend({
    increment: 5,
    postsLimit: function () {
        return parseInt(this.params.postsLimit) || this.increment;
    },
    findOptions: function () {
        return {sort: {submitted: -1}, limit: this.postsLimit()};
    },
    subscriptions: function () {
        Meteor.subscribe('images');
        this.postsSub = Meteor.subscribe('posts', this.filter(), this.findOptions());
    },
    posts: function () {
        return Posts.find(this.filter(), this.findOptions());
    },
    data: function () {
        var hasMore = this.posts().count() === this.postsLimit();
        return {
            posts: this.posts(),
            ready: this.postsSub.ready,
            nextPath: hasMore ? this.nextPath() : null
        };
    }
});
NewPostsController = PostsListController.extend({
    template: 'postDisplay',
    nextPath: function () {
        return Router.routes.newPosts.path({postsLimit: this.postsLimit() + this.increment})
    },
    filter: function () {
        if(Meteor.userId() === null){
            return {}
        }
        return {
            $nor: [
                {upVoters: {$all: [Meteor.userId()]}},
                {downVoters: {$all: [Meteor.userId()]}},
                {createdBy: Meteor.userId()}
            ]
        };
    }
});

Router.route('/', {
    name: 'home',
    controller: NewPostsController
});
Router.route('/new/:postsLimit?', {name: 'newPosts'});


MyPostsController = PostsListController.extend({
    template: 'myPosts',
    nextPath: function () {
        return Router.routes.myPosts.path({postsLimit: this.postsLimit() + this.increment})
    },
    filter: function () {
        return {
            createdBy: Meteor.userId()
        };
    }
});
Router.route('/myPosts/:postsLimit?', {
    name: 'myPosts',
    controller: MyPostsController
});

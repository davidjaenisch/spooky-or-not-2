Meteor.publish('posts', function(filter, options) {
    return Posts.find(filter, options);
});

Meteor.publish('singlePost', function(id){
    check(id, String);
    return Posts.find(id);
});

Meteor.publish('myPosts', function(id){
    check(id, String);
    return Posts.find({
        createdBy: id
    });
});

Meteor.publish('images', function() {
    return Images.find();
});